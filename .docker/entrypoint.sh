#!/bin/bash

echo "Iniciando instalacao npm" 
npm cache clean -f
npm install
npm run typeorm -- -d src/shared/infra/typeorm/index.ts migration:run
npm run dev
