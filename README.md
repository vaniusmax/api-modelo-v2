

Após clonar o conteúdo do repositório, acesse o diretório criado e efetue a instalação das dependências:

```
cd api-model-v2

yarn

# ou

npm install
```

Após essa instalação execute a aplicação com o comando `yarn dev` ou `npm run dev`. O servidor estará em execução no endereço `http://localhost:3333`.

Tambem pode-se utilizar o docker compose para subir a app de forma containerizada 

docker compose up


